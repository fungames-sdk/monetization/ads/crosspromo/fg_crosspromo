# Crosspromo

## Introduction

Crosspromo is used to promote internal apps within your game through specific UI.

## Integration Steps

1) **"Install"** or **"Upload"** FG Crosspromo plugin from the FunGames Integration Manager in Unity, or download it from here.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

## Scripting API

> You can use these functions to play and close a crosspromo ad :

```csharp
FGCrosspromo.Play();
FGCrosspromo.Close();
```

> You can also subscribe to these callbacks if you want to perform custom actions when an ad is played or when it is completed :

```csharp
FGCrosspromo.Callbacks.Initialization
FGCrosspromo.Callbacks.OnInitialized
FGCrosspromo.Callbacks.OnPlay;
FGCrosspromo.Callbacks.OnClose;
FGCrosspromo.Callbacks.OnCompleted;
```